# Python_Sandbox
OpenBCI and BrainFlow library sandbox.

## Install
### Prerequisite System
Ubuntu 18.04

Required by sounddevice
libportaudio2=19.6.0-1

### Setup teh environment
```
# Conda
$ conda update conda
$ conda create --name python_sandbox
$ conda activate python_sandbox
$ conda install python=3.8


# Python libs

# sounddevice provides bindings for the PortAudio library and a few convenience 
# functions to play and record NumPy arrays containing audio signals.
# https://python-sounddevice.readthedocs.io/

$ python -m pip install sounddevice


# Matplotlib is a comprehensive library for creating static, animated, 
# and interactive visualizations in Python.
# https://matplotlib.org/

$ python -m pip install matplotlib

```

## Start the software
```
$ conda activate python_sandbox
<cmd here>
```   
Example: 

## Useful commands

Get a list of your OS supported devices.
```
python3 -m sounddevice
```

